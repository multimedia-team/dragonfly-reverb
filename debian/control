Source: dragonfly-reverb
Section: sound
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders:
 Olivier Humbert <trebmuh@tuxfamily.org>,
 Dennis Braun <snd@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 dpf-source:native,
 libfftw3-dev,
 libgl-dev,
 libglu1-mesa-dev | libglu-dev,
 libjack-dev | libjack-jackd2-dev,
 liblo-dev,
 libsamplerate0-dev,
 libx11-dev,
 libxext-dev,
 libxrandr-dev,
 pkg-config,
Homepage: https://michaelwillis.github.io/dragonfly-reverb/
Vcs-Git: https://salsa.debian.org/multimedia-team/dragonfly-reverb.git
Vcs-Browser: https://salsa.debian.org/multimedia-team/dragonfly-reverb
Standards-Version: 4.6.2
Rules-Requires-Root: no

Package: dragonfly-reverb
Architecture: all
Multi-Arch: foreign
Depends:
 dragonfly-reverb-lv2,
 dragonfly-reverb-standalone,
 dragonfly-reverb-vst,
 dragonfly-reverb-vst3,
 dragonfly-reverb-clap,
 ${misc:Depends},
Provides:
 lv2-plugin,
 vst-plugin,
Description: Reverb effect plugins - metapackage
 Dragonfly Reverb is a bundle of free audio reverb effects:
  * Early Reflections
    - stereo-to-stereo reverb
  * Hall Reverb
    - stereo-to-stereo reverb
  * Plate
    - mono-to-stereo reverb
  * Room Reverb
    - stereo-to-stereo reverb
 .
 This metapackage installs all JACK, LV2, VST3, CLAP and VST plugins

Package: dragonfly-reverb-standalone
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: Reverb effect plugins - standalone applications
 Dragonfly Reverb is a bundle of free audio reverb effects:
  * Early Reflections
    - stereo-to-stereo reverb
  * Hall Reverb
    - stereo-to-stereo reverb
  * Plate
    - mono-to-stereo reverb
  * Room Reverb
    - stereo-to-stereo reverb
 .
 This package provides the standalone JACK applications

Package: dragonfly-reverb-vst
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Provides:
 vst-plugin,
Description: Reverb effect plugins - VST plugins
 Dragonfly Reverb is a bundle of free audio reverb effects:
  * Early Reflections
    - stereo-to-stereo reverb
  * Hall Reverb
    - stereo-to-stereo reverb
  * Plate
    - mono-to-stereo reverb
  * Room Reverb
    - stereo-to-stereo reverb
 .
 This package provides the VST plugins

Package: dragonfly-reverb-lv2
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Provides:
 lv2-plugin,
Description: Reverb effect plugins - LV2 plugins
 Dragonfly Reverb is a bundle of free audio reverb effects:
  * Early Reflections
    - stereo-to-stereo reverb
  * Hall Reverb
    - stereo-to-stereo reverb
  * Plate
    - mono-to-stereo reverb
  * Room Reverb
    - stereo-to-stereo reverb
 .
 This package provides the LV2 plugins

Package: dragonfly-reverb-vst3
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Provides:
 vst3-plugin,
Description: Reverb effect plugins - VST3 plugins
 Dragonfly Reverb is a bundle of free audio reverb effects:
  * Early Reflections
    - stereo-to-stereo reverb
  * Hall Reverb
    - stereo-to-stereo reverb
  * Plate
    - mono-to-stereo reverb
  * Room Reverb
    - stereo-to-stereo reverb
 .
 This package provides the VST3 plugins

Package: dragonfly-reverb-clap
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Provides:
 clap-plugin,
Description: Reverb effect plugins - CLAP plugins
 Dragonfly Reverb is a bundle of free audio reverb effects:
  * Early Reflections
    - stereo-to-stereo reverb
  * Hall Reverb
    - stereo-to-stereo reverb
  * Plate
    - mono-to-stereo reverb
  * Room Reverb
    - stereo-to-stereo reverb
 .
 This package provides the CLAP plugins
